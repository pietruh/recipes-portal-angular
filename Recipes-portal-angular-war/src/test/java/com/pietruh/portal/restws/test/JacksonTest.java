package com.pietruh.portal.restws.test;

import static org.junit.Assert.*;

import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

public class JacksonTest {

	@Test
	public void passUserObject_shouldReturnJson() throws Exception {
		User user = new User();
		Code code = new Code();
		List<Code> codes = new ArrayList<>();
		code.setId(1);
		code.setCd("code no 1");
		codes.add(code);
		codes.add(code);
		user.setAge(10);
		user.setId(1);
		user.setName("Asia");
		user.setCodes(codes);

		List<User> list = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		list.add(user);
		list.add(user);
		Writer writer = new StringWriter();
		mapper.writeValue(writer, list);
		System.out.println(writer.toString());

	}

	@Test
	public void testPolishCharacterBase64() throws Exception {
		byte[] binaryData = "ąśćół".getBytes();
		String eb64 = Base64.encodeBase64String(binaryData);
		byte[] db64 = Base64.decodeBase64(eb64);
		System.out.println(eb64);
		
		System.out.println(new String(db64,"UTF-8"));
	}

	@Data
	class User {
		private int id;
		private String name;
		private int age;
		private List<Code> codes;

		public User() {
		}

		@JsonCreator
		public User(@JsonProperty("id") int id, @JsonProperty("name") String name,
				@JsonProperty int age, @JsonProperty List<Code> codes) {
			this.id = id;
			this.name = name;
			this.age = age;
			this.codes = codes;
		}
	}

	@Data
	class Code {
		private int id;
		private String cd;

	}

}

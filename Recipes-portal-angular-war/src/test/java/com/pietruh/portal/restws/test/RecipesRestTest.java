package com.pietruh.portal.restws.test;

import java.util.ArrayList;

import javax.ejb.embeddable.EJBContainer;

import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.jboss.resteasy.plugins.server.resourcefactory.POJOResourceFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;

import com.pietruh.portal.restws.RecipesRest;
import com.pietruh.recipes.dao.api.interfaces.RecipeDao;
import com.pietruh.recipes.dao.api.model.Recipe;

public class RecipesRestTest {

	private static final String recipesUri = "/Recipes-portal-angular-war/api/recipes/";
	private static final String recipesUri1 = "/recipes/";

	@InjectMocks
	private RecipeDao recipeDao;

	// @Sp

	@Before
	public void before() throws Exception {
		// EJBContainer.createEJBContainer().getContext().bind("inject", this);
	}

	@Test
	public void whileCallRestWithNoParams_shouldReturnAllJsonFormatRecipes() throws Exception {
		RecipeDao recipeMock = Mockito.mock(RecipeDao.class);
		Mockito.when(recipeMock.getRecipes()).thenReturn(new ArrayList<Recipe>());

		Dispatcher dispatcher = MockDispatcherFactory.createDispatcher();
		POJOResourceFactory pojoResource = new POJOResourceFactory(RecipesRest.class);
		dispatcher.getRegistry().addResourceFactory(pojoResource);
		MockHttpRequest request = MockHttpRequest.get(recipesUri1);
		MockHttpResponse response = new MockHttpResponse();

		// dispatcher.invoke(request, response);
		// int status = response.getStatus();
		// Assert.assertEquals(200, status);
	}
}

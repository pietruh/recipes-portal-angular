package com.pietruh.websocket;

import java.util.HashSet;
import java.util.Set;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/hellowebsocket", decoders = HelloWebsocket.MyMessageDecoder.class)
public class HelloWebsocket {
	static Set<Session> peers = new HashSet<>();

	@OnOpen
	public void onOpen(Session session) {
		peers.add(session);
	}

	public void onClose(Session session) {
		peers.remove(session);
	}

	@OnMessage
	public String sayHello(String name, Session session) throws Exception {
		peers.stream().forEach((peer) -> {
			try {
				peer.getBasicRemote().sendObject(name);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		return "Hello " + name;
	}

	class MyMessageDecoder implements Decoder.Text<MyMessage> {

		@Override
		public void destroy() {
			// TODO Auto-generated method stub
		}

		@Override
		public void init(EndpointConfig arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public MyMessage decode(String arg0) throws DecodeException {
			// JSONObject jsonObject = Json.createReader("").readObject();
			return new MyMessage();
		}

		@Override
		public boolean willDecode(String arg0) {
			// TODO Auto-generated method stub
			return true;
		}

	}

	class MyMessage {

	}
}

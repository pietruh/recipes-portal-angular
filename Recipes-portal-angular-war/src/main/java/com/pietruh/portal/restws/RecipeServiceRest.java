package com.pietruh.portal.restws;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pietruh.recipes.dao.api.interfaces.RecipeDao;
import com.pietruh.recipes.dao.api.model.Recipe;

@Path("/recipe")
public class RecipeServiceRest {

	private static final Logger LOGGER = LoggerFactory.getLogger(RecipeServiceRest.class);
	private RecipeDao recipeDao;

	@Inject
	private void setRecipeDao(RecipeDao recipeDao) {
		this.recipeDao = recipeDao;
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecipe(@PathParam("id") String id) {
		String returnString = "";
		Recipe recipes = recipeDao.getRecipe(id);

		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Writer writer = new StringWriter();

			objectMapper.writeValue(writer, recipes);
			returnString = writer.toString();
			LOGGER.info(returnString);
		} catch (IOException e) {
			LOGGER.error("Error", e);
		}
		return Response.ok(returnString).build();
	}
}

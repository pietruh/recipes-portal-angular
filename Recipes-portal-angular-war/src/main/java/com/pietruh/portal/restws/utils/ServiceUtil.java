package com.pietruh.portal.restws.utils;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ServiceUtil {
	
	public static <T> T lookupClass(String jndi, Class<T> type) {
		try {
			Context context = new InitialContext(getProperties());
			return type.cast(context.lookup(jndi));
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static Hashtable<String, Object> getProperties() {

		Hashtable<String, Object> props = new Hashtable<>();
		props.put("endpoint.name", "client-endpoint");
		props.put(
				"remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED",
				false);
		props.put("remote.connections", "default");

		props.put("remote.connection.default.host", "localhost");
		props.put("remote.connection.default.port", "4447");
		props.put(
				"remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS",
				"false");

		props.put("remote.connection.default.username", "appuser");
		props.put("remote.connection.default.password", "apppassword");

		return props;
	}
}

package com.pietruh.portal.restws.producers;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.pietruh.portal.restws.utils.ServiceUtil;
import com.pietruh.recipes.dao.api.interfaces.CategoryDao;
import com.pietruh.recipes.dao.api.interfaces.RecipeDao;

public class RecipesDaoProducer {

	private static final String RECIPE_JNDI = "ejb:Recipes-dao-ear/Recipse-dao-ejb/RecipeDaoBean!com.pietruh.recipes.dao.api.interfaces.RecipeDao";
	private static final String CATEGORY_JNDI = "ejb:Recipes-dao-ear/Recipse-dao-ejb/CategoryDaoBean!com.pietruh.recipes.dao.api.interfaces.CategoryDao";

	private static final String MONGO_RECIPE_JNDI = "ejb:Recipes-mongodb-dao-ear/Recipse-mongodb-dao-ejb/RecipeDaoBean!com.pietruh.recipes.dao.api.interfaces.RecipeDao";
	private static final String MONGO_CATEGORY_JNDI = "ejb:Recipes-mongodb-dao-ear/Recipse-mongodb-dao-ejb/CategoryDaoBean!com.pietruh.recipes.dao.api.interfaces.CategoryDao";

	@Produces
	@ApplicationScoped
	public RecipeDao createRecipeDao() {
		return ServiceUtil.lookupClass(RecipesDaoProducer.MONGO_RECIPE_JNDI, RecipeDao.class);
	}

	@Produces
	@ApplicationScoped
	public CategoryDao createCategoryDao() {
		return ServiceUtil.lookupClass(RecipesDaoProducer.CATEGORY_JNDI, CategoryDao.class);
	}

}

package com.pietruh.portal.restws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.pietruh.recipes.dao.api.model.CuisineCategory;
import com.pietruh.recipes.dao.api.model.DifficultLevel;
import com.pietruh.recipes.dao.api.model.Weight;

@Path("enums")
public class EnumerationService {

	@GET
	@Path("difficultLevelTypes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDifficultLevel() {
		return Response.ok(DifficultLevel.values()).build();
	}

	@GET
	@Path("weightTypes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWeightType() {
		return Response.ok(Weight.values()).build();
	}
	
	@GET
	@Path("cuisineCategoryTypes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCuisineCategoryType() {
		return Response.ok(CuisineCategory.values()).build();
	}
	
	
}

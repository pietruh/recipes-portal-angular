package com.pietruh.portal.restws;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pietruh.recipes.dao.api.interfaces.RecipeDao;
import com.pietruh.recipes.dao.api.model.Recipe;

@Path("/recipes")
public class RecipesRest {

	private static final Logger LOGGER = LoggerFactory.getLogger(RecipesRest.class);
	private RecipeDao recipeDao;

	@Inject
	private void setRecipeDao(RecipeDao recipeDao) {
		this.recipeDao = recipeDao;
		Map<String, String> map =new HashMap<>();
		map.put("asdfs","asdf" );
		map.put("asdfs","asdf" );
		map.put("asdfs","asdf" );
		map.put("asdfs","asdf" );
		map.put("asdfs","fasdf");
		

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRecipes() {
		String returnString = "";
		List<Recipe> recipes = recipeDao.getRecipes();

		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Writer writer = new StringWriter();

			objectMapper.writeValue(writer, recipes);
			returnString = writer.toString();
			// LOGGER.info("Recipes=" + recipes);
		} catch (IOException e) {
			LOGGER.error("Error", e);
		}
		return Response.ok(returnString).build();
	}

	@GET
	@Path("user/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserRecipes(@PathParam("id") String id) {
		String returnString = "";
		int userId = Integer.valueOf(id);
		List<Recipe> recipes = recipeDao.getUserRecipes(userId);

		ObjectMapper objectMapper = new ObjectMapper();
		// objectMapper.

		// recipes.parallelStream()
		return Response.ok(returnString).build();
	}

	@Path("/datetime")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDate() {
		String datetime = new Date().toString();
		JSONObject obj = null;
		try {
			obj = new JSONObject();
			obj.putOpt("datetime", datetime);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String dateTimeReturn = "\"" + datetime + "\"";

		String str = "{\"datetime\":\"" + datetime + "\"}";
		String returnString = obj.toString();
		return Response.ok(returnString).build();
	}

	@Path("multiply/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMultiply(@PathParam("id") String id) {
		Integer idInt = Integer.valueOf(id);
		int mul = idInt * 10;
		// recipeDao.getUserRecipes(0);

		return Response.ok("\"" + mul + "\"").build();

	}

}

/**
 * 
 */
(function() {
	var app = angular.module('recipes-app', [ 'ngRoute' ]);
	// 'ngCookies', ,
	// 'ngSanitize'

	app.config(function($routeProvider, $locationProvider) {
		$routeProvider

		// route for the home page
		.when('/', {
			templateUrl : 'pages/home.html',
			controller : 'mainController'
		})

		// route for the about page
		.when('/login', {
			templateUrl : 'pages/login.html',
			controller : 'loginController'
		})

		// route for the contact page
		.when('/addrecipe', {
			templateUrl : 'pages/addrecipe.html',
			controller : 'addRecipeController'
		})
		// route for singin page
		.when('/sign-in', {
			templateUrl : 'pages/signin.html',
			controller : 'signinController'
		})

		.when('/recipe/:id', {
			templateUrl : function(params) {
				return 'pages/recipe.html';// + params.id;
			},
			controller : 'recipeController'
		});

		// $locationProvider.html5Mode(true);
	}).controller("mainController", function($scope, $http) {
		$scope.message = "test main"

		$http({
			method : 'GET',
			cache : true,
			url : '/Recipes-portal-angular-war/api/recipes'
		}).success(function(data, status, headers, config) {
			$scope.recipes = data;
			// ...
		}).error(function(data, status, headers, config) {
			// ...
		});

	}).controller("recipeController", function($scope, $http, $routeParams) {
		$scope.message = "Recipe test"

		$http({
			method : 'GET',
			cache : true,
			url : '/Recipes-portal-angular-war/api/recipe/' + $routeParams.id
		}).success(function(data, status, headers, config) {
			$scope.recipe = data;
			// ...
		}).error(function(data, status, headers, config) {
			// ...
		});

	}).controller("loginController", function($scope) {
		$scope.message = "login page"
	}).controller("addRecipeController", function($scope, $http) {
		$scope.message = "Przepis";

		$http({
			method : 'GET',
			cache : true,
			url : '/Recipes-portal-angular-war/api/enums/difficultLevelTypes'
		}).success(function(data, status, headers, config) {
			$scope.difficultLavelTypes = data;
			// ...
		}).error(function(data, status, headers, config) {
			// ...
		});
		$http({
			method : 'GET',
			cache : true,
			url : '/Recipes-portal-angular-war/api/enums/weightTypes'
		}).success(function(data, status, headers, config) {
			$scope.weightTypes = data;
			// ...
		}).error(function(data, status, headers, config) {
			// ...
		});

		$http({
			method : 'GET',
			cache : true,
			url : '/Recipes-portal-angular-war/api/enums/cuisineCategoryTypes'
		}).success(function(data, status, headers, config) {
			$scope.cuisineCategoryTypes = data;
			// ...
		}).error(function(data, status, headers, config) {
			// ...
		});

		$scope.recipe = {
			"name" : null,
			"cookingTime" : null,
			"dificultLavel" : null,
			"ingredients" : [ /*
								 * { "name" : "kapusta", "amount" : 11, "weight" :
								 * null }
								 */],
			"descriptions" : [ {
				"text" : "Lorem ipsum ibidem",
				'photo' : null
			} ],
			"cuisineCategory" : []
		};

		$scope.newIngredient = {
			"name" : "",
			"amount" : "",
			"weight" : ""
		};

		$scope.newDescription = {
			"text" : "",
			"photo" : ""
		};

		$scope.removeIngredint = function(ingridient) {
			var index = $scope.recipe.ingredients.indexOf(ingridient);
			if (index != -1) {
				$scope.recipe.ingredients.splice(index, 1);
			}
		};

		$scope.addIngredient = function(ingredient) {
			$scope.recipe.ingredients.push(ingredient);
			$scope.newIngredient = {};
		};

		$scope.removeDescription = function(description) {
			var index = $scope.recipe.descriptions.indexOf(description);
			if (index != -1) {
				$scope.recipe.descriptions.splice(index, 1);
			}
		};

		$scope.addDescription = function(description) {
			$scope.recipe.descriptions.push(description);
			$scope.newDescription = {};
		};

		$scope.toggleSelectionCuisine = function(cuisineCategory) {
			var idx = $scope.recipe.cuisineCategory.indexOf(cuisineCategory);

			if (idx > -1) {
				$scope.recipe.cuisineCategory.splice(idx, 1);
			}
			// is newly selected
			else {
				$scope.recipe.cuisineCategory.push(cuisineCategory);
			}
		};
	}).controller("signinController", function($scope) {
		$scope.message = "signin \n page"
	}).directive("menu", function() {
		return {
			restrict : "E",
			templateUrl : "partials/menu/menu.html"
		}
	});
})();
